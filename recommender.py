#!/usr/bin/env python
# coding: utf-8

import pandas as pd
import numpy as np
import neattext.functions as nfx
import matplotlib.pyplot as plt
import sys
import json

from sklearn.feature_extraction.text import CountVectorizer,TfidfVectorizer
from sklearn.metrics.pairwise import cosine_similarity,linear_kernel

df = pd.read_csv("./dataset.csv")
df['clean_course_title'] = df['name'].apply(nfx.remove_stopwords)
df['clean_course_title'] = df['clean_course_title'].apply(nfx.remove_special_characters)

count_vect = CountVectorizer()
cv_mat = count_vect.fit_transform(df['clean_course_title'])

cv_mat.todense()
df_cv_words = pd.DataFrame(cv_mat.todense(),columns=count_vect.get_feature_names())
cosine_sim_mat = cosine_similarity(cv_mat)
course_indices = pd.Series(df.index,index=df['name']).drop_duplicates()

def recommend_course(title,num_of_rec=10):
    # ID for title
    idx = course_indices[title]
    nump = np.int64(1)
    if(type(nump) != type(idx)):
        idx = idx.iloc[0]
    # Course Indice
    # Search inside cosine_sim_mat
    scores = list(enumerate(cosine_sim_mat[idx]))
    # Scores
    # Sort Scores
    sorted_scores = sorted(scores,key=lambda x:x[1],reverse=True)
    # Recommend 
    selected_course_indices = [i[0] for i in sorted_scores[1:]]
    selected_course_scores = [i[1] for i in sorted_scores[1:]]
    result = df.iloc[selected_course_indices]
    rec_df = pd.DataFrame(result)
    rec_df['similarity_scores'] = selected_course_scores
    data = rec_df.head(num_of_rec).to_dict(orient="record")

    # resp = { 
    #     "Response": 200,
    #     "Message": "Data from Python",
    #     "data": data
    # }

    # print(json.dumps(resp))
    # sys.stdout.flush()
    return data

data = [];

# for i in range(2, (sys.argv[1]+2)):
#     recommendation_array.append(recommend_course(sys.argv[i],3));


# resp = { 
#         "Response": 200,
#         "Message": "Data from Python",
#         "data": data
#     }

# print(json.dumps(resp))
# sys.stdout.flush()


args = [ 'recommender.py', 1, '3D Graphics in Android: Sensors and VR' ]
for i in range(2, (int(sys.argv[1])+2)):
    data.append(recommend_course(sys.argv[i],3));
resp = { 
        "Response": 200,
        "Message": "Data from Python",
        "data": data
    }
print(json.dumps(resp))
sys.stdout.flush()
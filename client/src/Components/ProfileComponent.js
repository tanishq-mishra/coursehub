import React, { useContext, useState, useEffect } from 'react'
import { AuthContext } from '../Context/AuthContext'
import AuthService from "../Services/AuthService";
import { withRouter } from "react-router";
import { motion } from 'framer-motion';
import { Link } from 'react-router-dom';
import { PageTransition, PageVariants } from "../Services/AnimationService";
export const Profile = (props) => {
    const [name, setName] = useState('');
    const [password, setPassword] = useState('');
    const [email, setEmail] = useState('');
    const [confirmPassword, setConfirmPassword] = useState('');
    const [newPassword, setNewPassword] = useState('');
    const [isValid, setIsValid] = useState({ value: true, msg: '' });
    const [msg, setMsg] = useState({ msg: '', error: false });
    const [changePassword, setChangePassword] = useState(false);



    const authContext = useContext(AuthContext);


    useEffect(() => {
        if (!authContext.isAuthenticated) {
            props.history.push('/')
        }

        setName(authContext.user.name);
        setEmail(authContext.user.email);


    }, []);


    const logoutHandler = (e) => {
        AuthService.logout();
        authContext.setUser({ name: "", email: "" });
        authContext.setIsAuthenticated(false);
        props.history.push('/');
    }
    const validate = (type, value) => {
        if (type === "confirmPassword") {
            if (value !== newPassword) {
                setIsValid({ value: false, msg: "Password Does Not Match" })
                return false;
            }
            else {
                setIsValid({ value: true, msg: "" })
                return true;
            }
        }
    }

    const handleChange = (e) => {
        if (e.target.name === 'email') {
            setEmail(e.target.value);
        }
        if (e.target.name === "password") {
            setPassword(e.target.value);
        } else if (e.target.name === "name") {
            setName(e.target.value);
        } else if (e.target.name === "confirmPassword") {
            setConfirmPassword(e.target.value);
        } else if (e.target.name === "newPassword") {
            setNewPassword(e.target.value)
        } else if (e.target.name === "changePassword") {
            setChangePassword(!changePassword);
        }
    }
    const handleBlur = (e) => {
        validate(e.target.name, e.target.value)
        e.target.addEventListener('change', (e) => {
            validate(e.target.name, e.target.value)
        })
    }
    const handleSubmit = (e) => {
        e.preventDefault();

        if (isValid.value) {
            AuthService.changeDetails({
                email: authContext.user.email,
                username: authContext.user.username,
                name: name.trim(),
                password: password,
                changePassword: changePassword,
                newPassword: newPassword
            }).then(data => {
                setMsg({ msg: data.msg, error: data.error });
            })
        }
    }

    return (
        <>
            <motion.div
                exit='out'
                initial='out'
                animate='in'
                variants={PageVariants}
                transition={PageTransition}
                className='dashboard-sidebar'>
                <div className="dashboard-top">
                    <Link to="/dashboard"><div className="dashboard-icon"><i style={{ color: 'white' }} className="fa fa-arrow-left font-lg"></i></div></Link>
                </div>
                <div className='dashboard-logout' onClick={logoutHandler}>
                    <i className="fa fa-sign-out fa-lg" aria-hidden="true"></i>
                </div>
            </motion.div>
            <motion.div
                exit='out'
                initial='out'
                animate='in'
                variants={PageVariants}
                transition={PageTransition}
                className='dashboard-container'>

                <div className='profile-container'>
                    <h1>Edit Profile</h1>
                    <div className="form profile-form">
                        <form onSubmit={handleSubmit}>
                            <input required={true} type="text" placeholder="Name" value={name} name="name" onChange={handleChange} onBlur={handleBlur} />
                            <input type="email" placeholder="Email" value={email} name="email" onBlur={handleBlur} onChange={handleChange} />
                            <input type="text" disabled={true} placeholder="Username" value={authContext.user.username} name="username" />
                            <div>
                                <input onChange={handleChange} className="checkbox" type="checkbox" name="changePassword" value={changePassword} />
                                <div>change password?</div>
                            </div>
                            {
                                changePassword ? <><input required={true} type="password" placeholder="New Password" value={newPassword} name="newPassword" onChange={handleChange} onBlur={handleBlur} />
                                    <input required={true} type="password" placeholder="Confirm Password" value={confirmPassword} name="confirmPassword" onChange={handleChange} onBlur={handleBlur} />
                                </> : null
                            }
                            <input required={true} type="password" placeholder="Password" value={password} name="password" onChange={handleChange} onBlur={handleBlur} />
                            {isValid.value === false ? <div className="form-error">{isValid.msg}</div> : <div></div>}
                            {(msg.msg.length === '' ? <div></div> : (msg.error === true ? <div className="msg error">{msg.msg}</div> : <div className="msg success">{msg.msg}</div>))}
                            <button className="register-button" type="submit">Save</button>
                        </form>
                    </div>

                </div>
            </motion.div>
        </>
    )
}


export default withRouter(Profile)
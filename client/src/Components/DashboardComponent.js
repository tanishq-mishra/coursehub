import React, { useContext, useState, useEffect, Component } from "react";
import { withRouter } from "react-router";
import Card from './ReusableCard';
import { AuthContext } from "../Context/AuthContext";
import AuthService from "../Services/AuthService";
import { motion, AnimatePresence } from 'framer-motion';

import { PageTransition, PageVariants, courseCardTransition, courseCardVariants, msgModalTransition, msgModalVariants } from "../Services/AnimationService";
import { Link } from "react-router-dom";

function Dashboard(props) {

    const authContext = useContext(AuthContext);
    const selectedCourseList = authContext.user.selectedCourses;
    const [courseData, setCourseData] = useState([]);
    const [recommendations, setRecommendations] = useState([]);
    const [showMessage, setShowMessage] = useState({ message: '', show: false });
    const [isLoading, setIsLoading] = useState(true);




    useEffect(() => {
        if (!authContext.isAuthenticated) {
            props.history.push('/')
        }
        const list = { slug: selectedCourseList };
        fetch('/getselected', {
            method: 'post',
            body: JSON.stringify(list),
            headers: {
                'Content-Type': 'application/json'
            }
        }).then(res => res.json()).then(data => {

            setCourseData(data.course);
            if (data.course.length !== 0) {
                let courseNames = [];
                data.course.map(d => {
                    courseNames.push(d.name);
                    return 0;
                })
                const selectedCourse = {
                    names: courseNames
                }
                fetch('/recommend', {
                    method: 'post',
                    body: JSON.stringify(selectedCourse),
                    headers: {
                        'Content-Type': 'application/json'
                    }
                }).then(res => res.json()).then(data => {
                    setRecommendations(data.recommendations.data)
                    setIsLoading(false);
                })
            }

        })

    }, []);

    const logoutHandler = (e) => {
        AuthService.logout();
        authContext.setUser({ name: "", email: "" });
        authContext.setIsAuthenticated(false);
        props.history.push('/');
    }

    const handleMail = (e) => {
        const data = {
            id: authContext.user._id
        }
        fetch('/sendmail', {
            method: 'POST',
            headers: {
                "Content-Type": "application/json"
            },
            body: JSON.stringify(data)
        }).then(res => res.json()).then(data => {
            setShowMessage({
                show: true,
                message: data.message.msgBody
            })

            setTimeout(() => {
                setShowMessage({
                    show: false,
                    message: ''
                })
            }, 3000)
        })
    }
    return (
        <>
            {isLoading ? <div id="loader">
                <div id="box"></div>
                <div id="hill"></div>
            </div> :
                <>
                    <AnimatePresence>
                        {showMessage.show ? <motion.div
                            exit="out"
                            initial="out"
                            animate='in'
                            variants={msgModalVariants}
                            transition={msgModalVariants}
                            className="msg-modal"
                        >
                            <div className="msg-modal-content" >{showMessage.message}</div>
                        </motion.div> : null}

                    </AnimatePresence>

                    <motion.div

                        initial='out'
                        animate='in'
                        variants={PageVariants}
                        transition={PageTransition}
                        className='dashboard-sidebar'>
                        <div className="dashboard-top">
                            <Link to="/profile"><div className="dashboard-icon"><i style={{ color: 'white' }} className="fa fa-cog fa-lg font-lg"></i></div></Link>
                            <div className='dashboard-email'><i className="fa fa-envelope fa-lg" onClick={handleMail} aria-hidden="true"></i></div>
                        </div>
                        <div className='dashboard-logout' onClick={logoutHandler}>
                            <i className="fa fa-sign-out fa-lg" aria-hidden="true"></i>
                        </div>
                    </motion.div>
                    <motion.div
                        exit='out'
                        initial='out'
                        animate='in'
                        variants={PageVariants}
                        transition={PageTransition}
                        className='dashboard-container'>

                        <div className='dashboard-main'>
                            {/* <div className='search-box-container'>
                        <input className="search-box" type="text" placeholder="Search Up!" autoFocus />
                        <button className="search-button">
                            <i className="fa fa-search fa-lg" aria-hidden="true"></i>
                        </button>
                    </div> */}
                            <div className='dashboard-welcome-message'>
                                {courseData.length === 0 ? `Hi ${authContext.user.name}, you have not selected any courses yet` : `Hi ${authContext.user.name}, these are the courses you added eariler...`}
                            </div>

                            <div className='dashboard-recommendations'>
                                <Card courseData={courseData} recommendation={false} />
                            </div>
                            {/* <div className='dashboard-welcome-message'>
                        {courseData.length === 0 ? null : `Here are a few recommendations for you`}
                    </div> */}


                            {recommendations.map((rec, index) => {
                                return (
                                    <>
                                        <div className='dashboard-welcome-message'>Because you liked {courseData[index].name}</div>
                                        <div className="recommendations-container dashboard-recommendations">
                                            <Card key={rec[0].name} recommendation={true} courseData={rec} />
                                        </div>
                                    </>
                                )
                            })}

                            <div className='dashboard-below-recommendations'>

                            </div>
                        </div>
                    </motion.div>
                </>}
        </>
    )

}

export default withRouter(Dashboard);
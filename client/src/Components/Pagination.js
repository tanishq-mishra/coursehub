import React from 'react'
export const Pagination = ({ postsPerPage, totalPosts, paginate, currentPage }) => {
    const pageNumbers = [];
    for (let i = 1; i <= Math.ceil(totalPosts / postsPerPage); ++i) {
        pageNumbers.push(i);

    }
    return (
        <div className='pagination-container'>
            <ul className='pagination'>
                {pageNumbers.map(number => {
                    const background = number === currentPage ? '#362657' : '#EFEFEF';
                    const color = number === currentPage ? '#EFEFEF' : '#362657'
                    return (
                        <li key={number}>
                            <button onClick={() => paginate(number)} style={{ color: color, backgroundColor: background }}>
                                {number}
                            </button>
                        </li>
                    )
                })}
            </ul>
        </div>
    )
}

export default Pagination;

const mongoose = require('mongoose');
const combined = new mongoose.Schema({
    courseProvider: String,
    courseType: String,
    description: String,
    photoUrl: String,
    courseId: String,
    url: String,
    name: String,
    metaphoneDescription: String,
    metaphoneName: String
});


module.exports = mongoose.model('Combined', combined);
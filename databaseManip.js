const mongoose = require('mongoose');

const Coursera = require('./coursera');
const Udemy = require('./udemy');
const Combined = require('./combined');
const coursera = require('./coursera');

mongoose.connect('mongodb://localhost/coursehub', { useNewUrlParser: true });
mongoose.connection.once('open', () => {
    console.log("connected to mongodb");
}).on('error', (err) => {
    console.log('connection error:', err)
});


// Coursera.find({ courseProvider: "Udacity" }, (err, doc) => {
//     if (err) {
//         console.log(err);
//     } else {
//         console.log(doc.length);
//         doc.map(async (d) => {

//             newCombined = new Combined({
//                 courseProvider: d.courseProvider,
//                 courseType: d.courseType,
//                 description: d.description,
//                 photoUrl: d.photoUrl,
//                 courseId: d.courseId,
//                 url: d.slug,
//                 name: d.name,
//                 metaphoneDescription: d.metaphoneDescription,
//                 metaphoneName: d.metaphoneName
//             });
//             await newCombined.save(err => {
//                 if (err) {
//                     console.log(err);
//                 }
//                 else {
//                     console.log("Doc Added")
//                 }
//             });
//         })
//     }
// })



Udemy.find({}, (err, doc) => {
    if (err) {
        console.log(err);
    } else {
        console.log(doc.length);
        doc.map(async (d) => {

            newCombined = new Combined({
                courseProvider: d.courseProvider,
                courseType: d.courseType,
                description: d.description,
                photoUrl: d.photoUrl,
                courseId: d.courseId,
                url: d.url,
                name: d.name,
                metaphoneDescription: d.metaphoneDescription,
                metaphoneName: d.metaphoneName
            });
            await newCombined.save(err => {
                if (err) {
                    console.log(err);
                }
                else {
                    console.log("Doc Added")
                }
            });
        })
    }
})
